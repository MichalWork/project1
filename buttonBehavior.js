var json = {
	'HTML': {
		'projctOne':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/247698_grafika_fantasy.jpg','link': 'www.google.com'},
		'projctTwo':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/247698_grafika_fantasy.jpg','link': 'www.google.com'},
		'projctThree':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/247698_grafika_fantasy.jpg','link': 'www.google.com'},
	},
	'CSS':{
		'projctOne':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-zly-wilk-w-grafice.jpg','link': 'www.onet.pl'},
		'projctTwo':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-zly-wilk-w-grafice.jpg','link': 'www.onet.pl'},
		'projctThree':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-zly-wilk-w-grafice.jpg','link': 'www.onet.pl'},
	},
	'JAVA SCRIPT':{
		'projctOne':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-bok-bugatti-divo.jpg','link': 'easy-code.io'},
		'projctTwo':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-bok-bugatti-divo.jpg','link': 'easy-code.io'},
		'projctThree':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-bok-bugatti-divo.jpg','link': 'easy-code.io'},
		'projctFour':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-bok-bugatti-divo.jpg','link': 'easy-code.io'},
		'projctFive':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-bok-bugatti-divo.jpg','link': 'easy-code.io'},
		'projctSix':{'localization': 'https://www.tapeciarnia.pl/tapety/normalne/tapeta-bok-bugatti-divo.jpg','link': 'easy-code.io'},
	},
	'REACT':{},
	
}

var projectsArea = document.querySelector(".section-projects__content");
var mainContentWrapper = document.querySelector('.main-content__wrapper');
var sectionAboutWrapper = mainContentWrapper.querySelector('.section-about__wrapper');
var inputs = document.querySelectorAll(".section-projects__nav input");

function createImg(localizationsImg, linkToProject){

	var a = document.createElement('a');
	//a.href = 'http://' + linkToProject;

	var newImg = new Image();
	newImg.src = localizationsImg;
	newImg.alt = 'brak zdjęcia';

	a.appendChild(newImg);
	projectsArea.appendChild(a);
 }

function buttonBehavior(language){
	for(let i in json[language]){
			createImg(json[language][i]['localization'], json[language][i]['link']);
		};
	}


buttonBehavior('HTML');


for(let input = 0; input< inputs.length; input++){
	inputs[input].addEventListener('click', function(){
			let oldProjects = projectsArea.querySelectorAll('a');
			for(let project = 0;  project < oldProjects.length; project++){
				oldProjects[project].remove();	
			}
			buttonBehavior(this.getAttribute('value'));
	})	
	
}