var sectionAboutWrapper = document.querySelector('div.section-about__wrapper');

function aboutBehavior(){
	let height = sectionAboutWrapper.offsetHeight;
	pageWidth= document.documentElement.offsetWidth;
	if(pageWidth > 470){
		sectionAboutWrapper.style.transform = 'translateY(-20em)';
	}else {
		sectionAboutWrapper.style.transform = 'translateY(-21em)';
		sectionAboutWrapper.style.height = 'unset'
	}

}

function animationBehavior(){
	const scroll = document.documentElement.scrollTop 
	if(scroll > 680){
		sectionAboutWrapper.style.transition = '3s transform linear, 3s height linear';
		aboutBehavior();
	}	
}
document.addEventListener('scroll', animationBehavior);